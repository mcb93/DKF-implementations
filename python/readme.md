Runs on Python 3.9.9 with requirements as outlined in `requirements.txt`.

For example,

```
python3 -m venv turquoise
source turquoise/bin/activate
pip3 install -r requirements.txt
python3 exampleUsage.py
```

Three separate runs produced normalized rmse of 0.6709, 0.6901, and 0.6676.
